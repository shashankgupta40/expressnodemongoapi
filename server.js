import express from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';

import bookRouter from './routes/bookRouter';

const app = express();
const port = process.env.PORT || 5656;

const db = mongoose.connect("mongodb://localhost/apitest", { useNewUrlParser: true })
    .then(() => {
        console.log("Successfully connected to the database");
    }).catch(err => {
        console.log('Could not connect to the database. Exiting now...');
        process.exit();
    });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/api/books', bookRouter); // This must be at last

app.listen(port, () => {
    console.log(`http://localhost:${port}`);
});