FROM node:latest
WORKDIR /express-server-app

#Adding relevant folders to image
ADD dist /express-server-app/dist
ADD node_modules /express-server-app/dist/node_modules

WORKDIR /express-server-app/dist

CMD ["node", "server.js"]

MAINTAINER shashank.gupta40@gmail.com