node and mongo must be installed

DB connection is in server.js
-----------To run--------------------------------
npm i

npm start
----------------------------------------------

GET : http://localhost:5656/api/books/<ID>

--------------------------------------------

POST : http://localhost:5656/api/books

{
	"title":"Ignited Minds","author":"APJ Abdul Kalam"
}

content-type : Application/json
raw
JSON(application/json)

---------------------------------------------------------------
Building docker image

$ npm run build

$ docker build -t express-node-mongo-api .

// Note --network host this is used to connect to localhost for connecting to mongo db

$ docker run -d --network host --name express-node-mongo-api express-node-mongo-api:latest

$ docker start express-node-mongo-api

// This will work in container as well as localhost



curl http://localhost:5656/api/books 
